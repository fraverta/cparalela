#!/bin/bash

# set the number of nodes
#SBATCH --nodes=1

# set max wallclock time
#SBATCH --time=02:00:00

# set name of job
#SBATCH --job-name=mejoras

nohup srun bash run.sh > output.txt 2>&1 &

