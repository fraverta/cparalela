#!/bin/bash

EXECUTE_CMD='perf_4.4 stat -r 5 ./heat '
declare -a COMPILERS=('gcc-8')
declare -a FLAGS=('-O3 -ffast-math -funroll-loops -ftree-vectorize')
declare -a SIZES=(1000 2000 3000 4000 5000)


for compiler in "${COMPILERS[@]}"
do
	for flag in "${FLAGS[@]}"
	do
		for N in "${SIZES[@]}"
		do
			for T in {12}
			do
			 	echo "["$N-$T"]"
				compile_cmd='make CC='"$compiler"' CFLAGS="-Wall -Wextra -std=c99 -g -march=native '"$flag"
				first_compilation="$compile_cmd -D_N_=$N\""  
				execute_cmd="export OMP_NUM_THREADS=$T && $EXECUTE_CMD >  $N-$T-1.txt 2> $N-$T-2.txt";
				echo $first_compilation;
				echo $execute_cmd;
				make clean && eval $first_compilation && eval "$execute_cmd";
			done
		done
	done
done

