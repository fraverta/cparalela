#!/bin/bash

EXECUTE_CMD='perf_4.16 stat -r 3 ./heat > output1.txt 2> output2.txt'
declare -a COMPILERS=('gcc-8')
declare -a FLAGS=('-O3 -ffast-math -funroll-loops -ftree-vectorize')
declare -a SIZES=(1000 2000 3000 4000 5000)

for compiler in "${COMPILERS[@]}"
do
	for flag in "${FLAGS[@]}"
	do
		for N in "${SIZES[@]}"
		do
		 	echo "[$N]"
			compile_cmd='make CC='"$compiler"' CFLAGS="-Wall -Wextra -std=c99 -g -march=native '"$flag"
			first_compilation="$compile_cmd -D_N_=$N\" OUTPUT=$N/"  
			echo $first_compilation;
			mkdir "$N";
			make clean; eval $first_compilation;
			cd "$N"; 
			eval "$EXECUTE_CMD"; 
			cd ..;
		done

	done
done

