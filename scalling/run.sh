for n in {1000..50000..1000}
do
	echo "[$Running N=$n]";
	compile_cmd="make CC='gcc-8' CFLAGS='-Wall -Wextra -std=c99 -g -march=native -O3 -ffast-math -funroll-loops -D_N_=$n'";
	make clean; eval $compile_cmd;
	for i in {0..19}
	do	
		echo "    [$Rep $i]";
		./heat >>OUTPUT-$n.txt 2>&1;
	done
done
