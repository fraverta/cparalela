#!/bin/bash

# set the number of nodes
#SBATCH --nodes=1

# set max wallclock time
#SBATCH --time=00:30:00

# set name of job
#SBATCH --job-name=mejoras
export CUDA_VISIBLE_DEVICES=0
nohup srun bash run.sh > output.txt 2>&1 &

