#!/bin/bash

EXECUTE_CMD='perf stat -r 3 ./heat > output1.txt 2> output2.txt'
declare -a ROWS_I=(6 8 10 12 14 16 18 20 22 24 26 28 30 32)


for rows in "${ROWS_I[@]}"
do
 	echo "[$rows x 16]"
	compile_cmd='make CFLAGS="-Wall -g -O3 -D_N_=1000 -D_BLOCK_SIZE_R_='$rows' -D_BLOCK_SIZE_C_=16 " OUTPUT='$rows'-16/'  
	echo $compile_cmd;
	mkdir "$rows-16";
	make clean; eval $compile_cmd;
	cd "$rows-16"; 
	eval "$EXECUTE_CMD"; 
	cd ..;
done

