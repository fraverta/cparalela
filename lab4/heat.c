#define _GNU_SOURCE
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>
#include <unistd.h>
#include <cuda_runtime.h>


#include "colormap.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

#if !defined(_N_)
    #define _N_ 500 
#endif
#if !defined(_SOURCE_TEMP_)
    #define _SOURCE_TEMP_ 5000.0f 
#endif
#if !defined(_BOUNDARY_TEMP_)
    #define _BOUNDARY_TEMP_ 2500.0f 
#endif
#if !defined(_MIN_DELTA_)
    #define _MIN_DELTA_ 0.05f 
#endif
#if !defined(_MAX_ITERATIONS_)
    #define _MAX_ITERATIONS_ 10000 
#endif
#if !defined(_SEED_)
    #define _SEED_ 0
#endif

#if defined(_TEST_)
    #define STRINGIZE(x) #x
    #define STRINGIZE_VALUE_OF(x) STRINGIZE(x)
#endif

#if !defined(_BLOCK_SIZE_R_)
    #define _BLOCK_SIZE_R_ 16
#endif

#if !defined(_BLOCK_SIZE_C_)
    #define _BLOCK_SIZE_C_ 16
#endif

// Simulation parameters
static const unsigned int N = _N_;

static const float SOURCE_TEMP = _SOURCE_TEMP_;
static const float BOUNDARY_TEMP = _BOUNDARY_TEMP_;

static const float MIN_DELTA = _MIN_DELTA_;
static const unsigned int MAX_ITERATIONS = _MAX_ITERATIONS_;

// It expect a sentence like DEFINE RANDOM_SEED X in compilation code
static const int SEED = _SEED_;

static const int BLOCK_SIZE_R = _BLOCK_SIZE_R_;
static const int BLOCK_SIZE_C = _BLOCK_SIZE_C_;

static unsigned int idx(unsigned int x, unsigned int y, unsigned int stride) {
    return y * stride + x;
}


static void init(unsigned int source_x, unsigned int source_y, float * matrix) {
    // init
    memset(matrix, 0, N * N * sizeof(float));

    // place source
    matrix[idx(source_x, source_y, N)] = SOURCE_TEMP;

    // fill borders
    for (unsigned int x = 0; x < N; ++x) {
        matrix[idx(x, 0,   N)] = BOUNDARY_TEMP;
        matrix[idx(x, N-1, N)] = BOUNDARY_TEMP;
    }
    for (unsigned int y = 0; y < N; ++y) {
        matrix[idx(0,   y, N)] = BOUNDARY_TEMP;
        matrix[idx(N-1, y, N)] = BOUNDARY_TEMP;
    }
}

// Kernel definition
__global__ void step(unsigned int source_x, unsigned int source_y, float * current, float * next)
{
		
    int y = blockIdx.x * blockDim.x + threadIdx.x + 1;
    int x = blockIdx.y * blockDim.y + threadIdx.y + 1;

		if ( x <= N-2 && y<=N-2 && (x !=source_x || y!=source_y) ){
		  float old_value = current[y*N + x];
			float new_value = (current[(y-1) * N + x] +
		                      current[y * N + x-1] +
		                      current[y * N + x+1] +
		                    current[(y+1) * N + x]) / 4.0f;
		  next[y * N + x] = new_value;
	}							
}

void write_png(float * current) {
    uint8_t * image = (uint8_t *) malloc(3 * N * N * sizeof(uint8_t));
    float maxval = fmaxf(SOURCE_TEMP, BOUNDARY_TEMP);

    for (unsigned int y = 0; y < N; ++y) {
        for (unsigned int x = 0; x < N; ++x) {
            unsigned int i = idx(x, y, N);
            colormap_rgb(COLORMAP_INFERNO, current[i], 0.0f, maxval, &image[3*i], &image[3*i + 1], &image[3*i + 2]);
        }
    }
    stbi_write_png("heat.png", N, N, 3, image, 3 * N);

    free(image);
}

#if defined(_TEST_)
void write_file(float * current){
    FILE *f = fopen(STRINGIZE_VALUE_OF(_TEST_), "w");
    if (f == NULL)
        printf("Error opening file!\n");

    /* printing single chatacters */
    for(unsigned int i = 0; i < N*N; i++){
        fprintf(f, "%f ", *(current + i));
        if ((i+1)%N == 0)
            fprintf(f, "\n");
    }
    fclose(f);
}
#endif

int main() {
    size_t array_size = N * N * sizeof(float);

    float * current = (float *) malloc(array_size);
    //float * next = malloc(array_size);
		
    srand(SEED);
    unsigned int source_x = rand() % (N-2) + 1;
    unsigned int source_y = rand() % (N-2) + 1;
    printf("Heat source at (%u, %u)\n", source_x, source_y);

    init(source_x, source_y, current);

    // Load current to device
		float * current_device;
    cudaMalloc(&current_device, array_size);
    cudaMemcpy(current_device, current, array_size, cudaMemcpyHostToDevice);


    // Load next to device
		float * next_device;
    cudaMalloc(&next_device, array_size);
    cudaMemcpy(next_device, current, array_size, cudaMemcpyHostToDevice);

		dim3 dimBlock(BLOCK_SIZE_R, BLOCK_SIZE_C);
		int blocks_per_grid_x = (N - 2 + dimBlock.x - 1)  / dimBlock.x;
		int blocks_per_grid_y = (N - 2 + dimBlock.y - 1)  / dimBlock.y;
	  dim3 dimGrid(blocks_per_grid_x, blocks_per_grid_y);

		double t = omp_get_wtime();
    unsigned int t_diff = 1;
		unsigned int it;
    for (it = 0; (it < MAX_ITERATIONS) && t_diff; ++it) {

		  // Invoke kernel
			step<<<dimGrid, dimBlock>>>(source_x, source_y, current_device, next_device);

      float * swap = current_device;
      current_device = next_device;
      next_device = swap;
    }

		 // Read C from device memory
    cudaMemcpy(current, current_device, array_size, cudaMemcpyDeviceToHost);
	
    double elapsed = omp_get_wtime() - t;
    double operations = it * 7 * (N-2) * (N-2);
    double gflops = operations / (1000.0 * 1000.0 * 1000.0 * elapsed); // 10^9 * elapsed (elapsed debe estar en seconds) GFLOPS
		printf("N: %d\n", _N_);    
		printf("Numero de iteraciones: %d\n", it);
		printf("Block size: %d x %d\n", BLOCK_SIZE_R, BLOCK_SIZE_C);
    printf("Elapsed: %f\n", elapsed);
    printf("%f GFLOPS\n", gflops);


    write_png(current);
		#if defined(_TEST_)
		write_file(current);
		#endif
		
		// Free device memory
    cudaFree(current_device);
    cudaFree(next_device);

    free(current);
    //free(next);

    return 0;
}
