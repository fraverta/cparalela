#!/bin/bash

if [ "$#" -ne 2 ]; then
	echo "Incorrect parameter number. Use bash run.sh C-COMPILER NUMBER_OF_PERF_REPETITIONS"
	exit 1;
fi

CC="$1"
CFLAGS="-Wall -Wextra -std=c99 -g -march=native -O3 -ffast-math -funroll-loops"
REP=$2

for i in {0..5}
do
	 echo "[Running $i]";
   cd $i; bash run.sh "$CC" "$CFLAGS" $REP; cd ..; 
done
echo "END!"
