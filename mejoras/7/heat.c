// make clean; make CFLAGS="-Wall -Wextra -std=c99 -g -march=native -O3 -ffast-math -funroll-loops"
// perf stat -e cpu-clock,cpu-cycles,page-faults,cache-references,cache-misses,instructions,stalled-cycles-frontend -r 20 ./heat
// make clean; make CFLAGS="-Wall -Wextra -std=c99 -g -march=native -O3 -ffast-math -funroll-loops -D_TEST_=result.txt"


#define _GNU_SOURCE
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>

#include "colormap.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

#if !defined(_N_)
    #define _N_ 500 
#endif
#if !defined(_SOURCE_TEMP_)
    #define _SOURCE_TEMP_ 5000.0f 
#endif
#if !defined(_BOUNDARY_TEMP_)
    #define _BOUNDARY_TEMP_ 2500.0f 
#endif
#if !defined(_MIN_DELTA_)
    #define _MIN_DELTA_ 0.05f 
#endif
#if !defined(_MAX_ITERATIONS_)
    #define _MAX_ITERATIONS_ 10000 
#endif
#if !defined(_SEED_)
    #define _SEED_ 0
#endif

#if defined(_TEST_)
    #define STRINGIZE(x) #x
    #define STRINGIZE_VALUE_OF(x) STRINGIZE(x)
#endif

// Simulation parameters
static const unsigned int N = _N_;

static const float SOURCE_TEMP = _SOURCE_TEMP_;
static const float BOUNDARY_TEMP = _BOUNDARY_TEMP_;

static const float MIN_DELTA = _MIN_DELTA_;
static const unsigned int MAX_ITERATIONS = _MAX_ITERATIONS_;

// It expect a sentence like DEFINE RANDOM_SEED X in compilation code
static const int SEED = _SEED_;

static unsigned int idx(unsigned int x, unsigned int y, unsigned int stride) {
    return y * stride + x;
}


static void init(unsigned int source_x, unsigned int source_y, float * matrix) {
    // init
    memset(matrix, 0, N * N * sizeof(float));

    // place source
    matrix[idx(source_x, source_y, N)] = SOURCE_TEMP;

    // fill borders
    for (unsigned int x = 0; x < N; ++x) {
        matrix[idx(x, 0,   N)] = BOUNDARY_TEMP;
        matrix[idx(x, N-1, N)] = BOUNDARY_TEMP;
    }
    for (unsigned int y = 0; y < N; ++y) {
        matrix[idx(0,   y, N)] = BOUNDARY_TEMP;
        matrix[idx(N-1, y, N)] = BOUNDARY_TEMP;
    }
}


static bool step(unsigned int source_x, unsigned int source_y, const float * current, float * next) {
		bool result = 0;
    for (unsigned int y = 1; y < N-1; ++y) {
        for (unsigned int x = 1; x < N-1; ++x) {
						float old_value = current[idx(x, y, N)];
						float new_value = (current[idx(x, y-1, N)] +
                                  current[idx(x-1, y, N)] +
                                  current[idx(x+1, y, N)] +
                                  current[idx(x, y+1, N)]) / 4.0f;
            next[idx(x, y, N)] = new_value;
						result = result || (fabsf(new_value - old_value) > MIN_DELTA);							
        }
    }
		next[idx(source_x, source_y, N)] = SOURCE_TEMP;	
		return result;
}


void write_png(float * current) {
    uint8_t * image = malloc(3 * N * N * sizeof(uint8_t));
    float maxval = fmaxf(SOURCE_TEMP, BOUNDARY_TEMP);

    for (unsigned int y = 0; y < N; ++y) {
        for (unsigned int x = 0; x < N; ++x) {
            unsigned int i = idx(x, y, N);
            colormap_rgb(COLORMAP_INFERNO, current[i], 0.0f, maxval, &image[3*i], &image[3*i + 1], &image[3*i + 2]);
        }
    }
    stbi_write_png("heat.png", N, N, 3, image, 3 * N);

    free(image);
}

#if defined(_TEST_)
void write_file(float * current){
    FILE *f = fopen(STRINGIZE_VALUE_OF(_TEST_), "w");
    if (f == NULL)
        printf("Error opening file!\n");

    /* printing single chatacters */
    for(unsigned int i = 0; i < N*N; i++){
        fprintf(f, "%f ", *(current + i));
        if ((i+1)%N == 0)
            fprintf(f, "\n");
    }
    fclose(f);
}
#endif

int main() {
    size_t array_size = N * N * sizeof(float);

    float * current = malloc(array_size);
    float * next = malloc(array_size);
		
    srand(SEED);
    unsigned int source_x = rand() % (N-2) + 1;
    unsigned int source_y = rand() % (N-2) + 1;
    printf("Heat source at (%u, %u)\n", source_x, source_y);

    init(source_x, source_y, current);
    memcpy(next, current, array_size);

    bool t_diff = 1;
    for (unsigned int it = 0; (it < MAX_ITERATIONS) && t_diff; ++it) {
        t_diff = step(source_x, source_y, current, next);
        //t_diff = diff(current, next);
        //printf("%u: %f\n", it, t_diff);

        float * swap = current;
        current = next;
        next = swap;
    }

    write_png(current);
		#if defined(_TEST_)
		write_file(current);
		#endif
    free(current);
    free(next);

    return 0;
}
