import sys
import subprocess, shlex
import time

COMPILERS = ['clang']
FLAGS = ['','-O1','-O2','-O3', '-O3 -ffast-math', '-O3 -funroll-loops', '-O3 -ffast-math -funroll-loops']
#TEST_PARAMETERS = {'N':1000,'SOURCE_TEMP':'5000.0f','BOUNDARY_TEMP':'2500.0f','MIN_DELTA':'0.05f','MAX_ITERATIONS':10000, 'D_SEED_':0}
EXECUTE_CMD = 'perf stat -e cpu-clock,cpu-cycles,page-faults,cache-references,cache-misses,instructions,stalled-cycles-frontend -r 20 ./heat'
EXPERIMENT_TARGET_FOLDER_PATH = '/home/nando/development/cParalela/cparalela/lab1/heat'

start_time = time.time()
now = time.strftime("%c")
#sys.stdout = open(EXPERIMENT_TARGET_FOLDER_PATH+'/runOLevel-output-%s.txt'%''.join(now.split()), 'w+')

print('Started at: %s'%now)
for compiler in COMPILERS:
	i=0
	for flag in FLAGS:
		print("[COMPILER %s FLAGS=%s]" %(compiler,flag))
		#COMPILE
		compile_cmd = 'make CC=%s CFLAGS="-Wall -Wextra -std=c99 -g -march=native %s '%(compiler, flag) 
		first_compilation = compile_cmd + ' -D_TEST_=%d/result.txt" OUTPUT=%d/'%(i,i)
		second_compilation = compile_cmd + '" OUTPUT=%d/'%i  
		try:
			subprocess.check_output(shlex.split('mkdir %d'%i))
			print("\t Compile and running for test")
			subprocess.check_output(shlex.split('make clean'))
			subprocess.check_output(shlex.split(first_compilation))
			subprocess.check_output(shlex.split('./heat'),cwd='%d/'%i)			
			print '\t Compile and running with perf'
			subprocess.check_output(shlex.split('make clean'))
			subprocess.check_output(shlex.split(second_compilation))
			process = subprocess.Popen(shlex.split(EXECUTE_CMD),cwd='%d/'%i, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
			stdout_output, stderr_output = process.communicate()
			with open('%i/output.txt'%i, 'w') as f:
				print >> f, stderr_output
		except:
			print '[Error] %s'%str(sys.exc_info())
			continue;
		i+=1
elapsed_time = time.time() - start_time
print('Ended at: %s. It takes %s'%(time.strftime("%c"),elapsed_time))

