// make clean; make CFLAGS="-Wall -Wextra -std=c99 -g -march=native -O3 -ffast-math -funroll-loops"
// perf stat -e cpu-clock,cpu-cycles,page-faults,cache-references,cache-misses,instructions,stalled-cycles-frontend -r 20 ./heat
// make clean; make CFLAGS="-Wall -Wextra -std=c99 -g -march=native -O3 -ffast-math -funroll-loops -D_TEST_=result.txt"


#define _GNU_SOURCE
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <x86intrin.h>
#include <immintrin.h>
#include <omp.h>


#include "colormap.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

#if !defined(_N_)
    #define _N_ 500 
#endif
#if !defined(_SOURCE_TEMP_)
    #define _SOURCE_TEMP_ 5000.0f 
#endif
#if !defined(_BOUNDARY_TEMP_)
    #define _BOUNDARY_TEMP_ 2500.0f 
#endif
#if !defined(_MIN_DELTA_)
    #define _MIN_DELTA_ 0.05f 
#endif
#if !defined(_MAX_ITERATIONS_)
    #define _MAX_ITERATIONS_ 10000 
#endif
#if !defined(_SEED_)
    #define _SEED_ 0
#endif

#if defined(_TEST_)
    #define STRINGIZE(x) #x
    #define STRINGIZE_VALUE_OF(x) STRINGIZE(x)
#endif


#define SIZE_M N-2

// Simulation parameters
static const unsigned int N = _N_;

static const float SOURCE_TEMP = _SOURCE_TEMP_;
static const float BOUNDARY_TEMP = _BOUNDARY_TEMP_;

static const float MIN_DELTA = _MIN_DELTA_;
static const unsigned int MAX_ITERATIONS = _MAX_ITERATIONS_;

// It expect a sentence like DEFINE RANDOM_SEED X in compilation code
static const int SEED = _SEED_;

//Number of real cols to generate an aligned matrix
static const unsigned int COLS = (_N_- 2) + (((_N_ - 2)%4 == 0)?0 : (4 -(_N_ - 2) % 4)) ;

static const unsigned int COL_MINUS_1 = ((_N_-2)%4 == 0)? ((_N_-2)/4 - 1) * 4: ((_N_-2)/4) * 4;

static unsigned int idx(unsigned int x, unsigned int y, unsigned int stride) {
    return y * stride + x;
}


static void init(unsigned int source_x, unsigned int source_y, float * matrix) {
    // init
    memset(matrix, 0, (N-2) * COLS * sizeof(float));

    // place source
    matrix[idx(source_x, source_y, COLS)] = SOURCE_TEMP;
}

unsigned int prom_vectors_and_save(__m128 a, __m128 b, __m128 c, __m128 d,__m128 old_value,float * mem_addr){
	__m128 result = _mm_add_ps(a,b);
	result = _mm_add_ps(result,c);
	result = _mm_add_ps(result,d);
	result = _mm_div_ps(result,_mm_setr_ps(4.0f,4.0f,4.0f,4.0f));

	
	__m128 diff = _mm_sub_ps(result, old_value);
	_mm_store_ps(mem_addr, result);
	return (fabsf(diff[0]) > MIN_DELTA) + (fabsf(diff[1]) > MIN_DELTA) + (fabsf(diff[2]) > MIN_DELTA) + (fabsf(diff[3]) > MIN_DELTA) ;
}

unsigned int lprom_vectors_and_save(__m128 a, __m128 b, __m128 c, __m128 d,__m128 old_value,float * mem_addr){
	__m128 result = _mm_add_ps(a,b);
	result = _mm_add_ps(result,c);
	result = _mm_add_ps(result,d);
	result = _mm_div_ps(result,_mm_setr_ps(4.0f,4.0f,4.0f,4.0f));

	
	__m128 diff = _mm_sub_ps(result, old_value);
	_mm_store_ps(mem_addr, result);
	
	#if SIZE_M%4 == 0
		return (fabsf(diff[0]) > MIN_DELTA) + (fabsf(diff[1]) > MIN_DELTA) + (fabsf(diff[2]) > MIN_DELTA) + (fabsf(diff[3]) > MIN_DELTA);
	#elif SIZE_M%4 == 1
		return (fabsf(diff[0]) > MIN_DELTA);
	#elif SIZE_M%4 == 2
		return (fabsf(diff[0]) > MIN_DELTA) + (fabsf(diff[1]) > MIN_DELTA) ;
	#else 
		return (fabsf(diff[0]) > MIN_DELTA) + (fabsf(diff[1]) > MIN_DELTA) + (fabsf(diff[2]) > MIN_DELTA);
	#endif

}

unsigned int compute_normal_cell(const float * current, float * next, unsigned int i, unsigned int j){
	__m128 above,below,left,right, old_value;
	above = _mm_load_ps (current + idx(j,i-1,COLS));
	left = _mm_loadu_ps( current + idx(j-1,i,COLS));
	old_value = _mm_load_ps( current + idx(j,i,COLS));			
	right = _mm_loadu_ps( current + idx(j+1,i,COLS));
	below = _mm_load_ps (current + idx(j,i+1,COLS));
	return prom_vectors_and_save(above, left, right, below, old_value, next + idx(j,i,COLS));
}

int compute_first4(const float * current, float * next, unsigned int i){
	__m128 above,below,left,right, old_value;
	above = _mm_load_ps (current + idx(0,i-1,COLS));
	left = _mm_loadu_ps( current + idx(0,i,COLS));
	left = _mm_castsi128_ps(_mm_slli_si128(_mm_castps_si128(left), 4)); 
	left[0] = BOUNDARY_TEMP;
	old_value = _mm_load_ps( current + idx(0,i,COLS));
	right = _mm_loadu_ps( current + idx(1,i,COLS));
	below = _mm_load_ps ( current + idx(0,i+1,COLS) );
	return prom_vectors_and_save(above, left, right, below, old_value, next + idx(0,i,COLS));
}

unsigned int compute_ffirst4(const float * current, float * next){
	__m128 above,below,left,right, old_value;
	above = _mm_setr_ps(BOUNDARY_TEMP,BOUNDARY_TEMP,BOUNDARY_TEMP,BOUNDARY_TEMP);
	left = _mm_loadu_ps( current + idx(0,0,COLS));
	left = _mm_castsi128_ps(_mm_slli_si128(_mm_castps_si128(left), 4)); 
	left[0] = BOUNDARY_TEMP;
	old_value = _mm_load_ps( current + idx(0,0,COLS));
	right = _mm_loadu_ps( current + idx(1,0,COLS));
	below = _mm_load_ps ( current + idx(0,1,COLS) );
	return prom_vectors_and_save(above, left, right, below, old_value, next + idx(0,0,COLS));
}

unsigned int compute_lfirst4(const float * current, float * next){
	__m128 above,below,left,right, old_value;
	above = _mm_load_ps (current + idx(0,SIZE_M-2,COLS));
	left = _mm_loadu_ps( current + idx(0,SIZE_M-1,COLS));
	left = _mm_castsi128_ps(_mm_slli_si128(_mm_castps_si128(left), 4)); 
	left[0] = BOUNDARY_TEMP;
	old_value = _mm_load_ps( current + idx(0,SIZE_M-1,COLS));
	right = _mm_loadu_ps( current + idx(1,SIZE_M-1,COLS));
	below = _mm_setr_ps(BOUNDARY_TEMP,BOUNDARY_TEMP,BOUNDARY_TEMP,BOUNDARY_TEMP);
	return prom_vectors_and_save(above, left, right, below, old_value, next + idx(0,SIZE_M-1,COLS));
}

unsigned int compute_last4(const float * current, float * next, unsigned int i){
		__m128 above,below,left,right, old_value;
		above = _mm_load_ps ( current + idx(COL_MINUS_1,i-1,COLS));
		left = _mm_loadu_ps( current + idx(COL_MINUS_1 - 1, i, COLS));
		old_value = _mm_load_ps( current + idx(COL_MINUS_1,i,COLS));
		right = _mm_loadu_ps( current + idx(COL_MINUS_1, i, COLS));
		right = _mm_castsi128_ps(_mm_srli_si128(_mm_castps_si128(right), 4)); 
		right[(3 + SIZE_M%4)%4] = BOUNDARY_TEMP;
		below = _mm_load_ps ( current + idx(COL_MINUS_1, i+1, COLS));	
	
		return lprom_vectors_and_save(above, left, right, below, old_value, next + idx(COL_MINUS_1, i, COLS));
}

unsigned int compute_flast4(const float * current, float * next){
		__m128 below,above,left,right, old_value;
		above = _mm_setr_ps(BOUNDARY_TEMP,BOUNDARY_TEMP,BOUNDARY_TEMP,BOUNDARY_TEMP);
		left = _mm_loadu_ps( current + idx(COL_MINUS_1 - 1, 0, COLS) );
		old_value = _mm_loadu_ps( current + idx(COL_MINUS_1,0,COLS));		
		right = _mm_loadu_ps( current + idx(COL_MINUS_1, 0, COLS));
		right = _mm_castsi128_ps(_mm_srli_si128(_mm_castps_si128(right), 4)); 
		right[(3 + SIZE_M%4)%4] = BOUNDARY_TEMP;
		below = _mm_load_ps ( current + idx(COL_MINUS_1, 1, COLS));	
	
		return lprom_vectors_and_save(above, left, right, below, old_value, next + idx(COL_MINUS_1, 0, COLS));
}


unsigned int compute_llast4(const float * current, float * next){
		__m128 below,above,left,right, old_value;
		above = _mm_load_ps ( current + idx(COL_MINUS_1, SIZE_M-1, COLS));
		left = _mm_loadu_ps( current + idx(COL_MINUS_1-1, SIZE_M-1, COLS) );
		old_value = _mm_load_ps( current + idx(COL_MINUS_1,SIZE_M - 1,COLS));
		right = _mm_loadu_ps( current + idx(COL_MINUS_1, SIZE_M-1, COLS));
		right = _mm_castsi128_ps(_mm_srli_si128(_mm_castps_si128(right), 4)); 
		right[(3 + SIZE_M%4)%4] = BOUNDARY_TEMP;
		below = _mm_setr_ps(BOUNDARY_TEMP,BOUNDARY_TEMP,BOUNDARY_TEMP,BOUNDARY_TEMP);

		return lprom_vectors_and_save(above, left, right, below, old_value, next + idx(COL_MINUS_1, SIZE_M-1, COLS));
}

static unsigned int step(unsigned int source_x, unsigned int source_y, const float * current, float * next) {
	unsigned int diff=0;
	__m128 above,below,left,right, old_value;
	
	// Work with first row as special case
	diff+=compute_ffirst4(current,next);
	above = _mm_setr_ps(BOUNDARY_TEMP,BOUNDARY_TEMP,BOUNDARY_TEMP,BOUNDARY_TEMP);	
	for(unsigned int j=4; j<COL_MINUS_1; j+=4){
			left = _mm_loadu_ps( current + idx(j-1,0,COLS) );			
			old_value = _mm_load_ps( current + idx(j,0,COLS));
			right = _mm_loadu_ps( current + idx(j+1,0,COLS) );
			below = _mm_load_ps ( current + idx(j,1,COLS) );

			diff+=prom_vectors_and_save(above, left, right, below, old_value, next + j);
	}
	diff+=compute_flast4(current,next);
	
	
	for(unsigned int i=1; i<N-3; i+=1){	
		diff+=compute_first4(current,next,i);
		for(unsigned int j=4; j<COL_MINUS_1; j+=4){
			//printf("%d %d\n",i,j);
			diff+=compute_normal_cell(current, next, i, j);
		}
		diff+=compute_last4(current,next,i);
	}
	
	// Work with last row as special case
	diff+=compute_lfirst4(current,next);	
	below = _mm_setr_ps(BOUNDARY_TEMP,BOUNDARY_TEMP,BOUNDARY_TEMP,BOUNDARY_TEMP);
	for(unsigned int j=4; j<COL_MINUS_1; j+=4){
			above = _mm_load_ps ( current + idx(j, SIZE_M-2, COLS));
			left = _mm_loadu_ps( current + idx(j-1, SIZE_M-1, COLS) );	
			old_value = _mm_load_ps( current + idx(j,SIZE_M-1,COLS));		
			right = _mm_loadu_ps( current + idx(j+1, SIZE_M-1, COLS) );

			diff+=prom_vectors_and_save(above, left, right, below, old_value, next + idx(j,SIZE_M-1,COLS));
	}
	diff+=compute_llast4(current,next);
	
	diff -= fabsf(next[idx(source_x, source_y, N)] -  current[idx(source_x, source_y, N)]) > MIN_DELTA;
	next[idx(source_x, source_y, COLS)] = SOURCE_TEMP;
	
	return diff;
}


void write_png(float * current) {
    uint8_t * image = malloc(3 * N * N * sizeof(uint8_t));
    float maxval = fmaxf(SOURCE_TEMP, BOUNDARY_TEMP);
		
		//Print first BOUNDARY_TEMP row
		for (unsigned int x = 0; x < N; ++x){
			colormap_rgb(COLORMAP_INFERNO, BOUNDARY_TEMP, 0.0f, maxval, &image[3*x], &image[3*x + 1], &image[3*x + 2]);
		}
    for (unsigned int y = 0; y < N-2; ++y) {
				unsigned int j = idx(0, y+1, N);
				colormap_rgb(COLORMAP_INFERNO, BOUNDARY_TEMP, 0.0f, maxval, &image[3*j], &image[3*j + 1], &image[3*j + 2]);
        for (unsigned int x = 0; x < N-2; ++x) {
            unsigned int i = idx(x, y, COLS);
						j = idx(x+1, y+1, N);
            colormap_rgb(COLORMAP_INFERNO, current[i], 0.0f, maxval, &image[3*j], &image[3*j + 1], &image[3*j + 2]);
        }
				j = idx(N-1, y+1, N);
				colormap_rgb(COLORMAP_INFERNO, BOUNDARY_TEMP, 0.0f, maxval, &image[3*j], &image[3*j + 1], &image[3*j + 2]);
    }

		//Print last BOUNDARY_TEMP row
		for (unsigned int x = 0; x < N; ++x){
			unsigned int j = idx(x, N-1, N);		
			colormap_rgb(COLORMAP_INFERNO, BOUNDARY_TEMP, 0.0f, maxval, &image[3*j], &image[3*j + 1], &image[3*j + 2]);
		}
    stbi_write_png("heat.png", N, N, 3, image, 3 * N);
    free(image);
}

#if defined(_TEST_)
void write_file(float * current){
    FILE *f = fopen(STRINGIZE_VALUE_OF(_TEST_), "w");
    if (f == NULL)
        printf("Error opening file!\n");

    /* printing single chatacters */
    for(unsigned int i = 0; i < N*N; i++){
        fprintf(f, "%f ", *(current + i));
        if ((i+1)%N == 0)
            fprintf(f, "\n");
    }
    fclose(f);
}
#endif

int main() {
		printf("COLS %d\n",COLS);
		printf("COLS_MINUS1 %d\n",COL_MINUS_1);
    size_t array_size = (N-2) * COLS * sizeof(float);


    float * current = malloc(array_size);
    float * next = malloc(array_size);


    srand(SEED);
    unsigned int source_x = rand() % (N-2);
    unsigned int source_y = rand() % (N-2);
    printf("Heat source at (%u, %u)\n", source_x + 1, source_y + 1);

    init(source_x, source_y, current);
    memcpy(next, current, array_size);
		
		double t = omp_get_wtime();
    unsigned int t_diff = 1;
		unsigned int it;
		//compute_normal_cell(current, next, 9997, 5404);
		
    for (it = 0; (it < MAX_ITERATIONS) && t_diff; ++it) {
				//printf("It: %d\n",it);
        t_diff = step(source_x, source_y, current, next);
        //t_diff = diff(current, next);
        //printf("%u: %f\n", it, t_diff);

        float * swap = current;
        current = next;
        next = swap;
    }
		
    double elapsed = omp_get_wtime() - t;
    double operations = it * 6 * (N-2) * (N-2);
    double gflops = operations / (1000.0 * 1000.0 * 1000.0 * elapsed); // 10^9 * elapsed (elapsed debe estar en seconds) GFLOPS
    printf("Elapsed: %f\n", elapsed);
    printf("Numero de iteraciones: %d\n", it);
    printf("%f GFLOPS\n", gflops);

		
    write_png(current);
		#if defined(_TEST_)
		write_file(current);
		#endif
    free(current);
    free(next);

    return 0;
}
