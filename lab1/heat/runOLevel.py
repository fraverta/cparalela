import sys
import subprocess, shlex
import time

COMPILERS = ['gcc-4.8','gcc-6']
FLAGS = ['-O1','-O2','-O3', '-O3 -ffast-math', '-O3 -funrool-loops', '-O3 -ffast-math -funroll-loops']
TEST_PARAMETERS = {'N':5,'SOURCE_TEMP':'5000.0f','BOUNDARY_TEMP':'2500.0f','MIN_DELTA':'0.005f','MAX_ITERATIONS':10000, 'D_SEED_':0}
EXECUTE_CMD = 'perf stat -e cpu-clock,cpu-cycles,page-faults,cache-references,cache-misses,instructions,stalled-cycles-frontend -r 20 ./heat'
EXPERIMENT_TARGET_FOLDER_PATH = '/home/nando/development/cParalela/cparalela/lab1/heat'

start_time = time.time()
now = time.strftime("%c")
sys.stdout = open(EXPERIMENT_TARGET_FOLDER_PATH+'/runOLevel-output-%s.txt'%''.join(now.split()), 'w+')

print('Started at: %s'%now)
for compiler in COMPILERS:
	for flag in FLAGS:
		print("[COMPILER %s FLAGS=%s]" %(compiler,flag))
		#COMPILE
		compile_cmd = 'make CC=%s CFLAGS="-Wall -Wextra -std=c99 -g -march=native %s'%(compiler, flag) 
		test_case_compile_cmd = compile_cmd + ' -D_TEST_=0 -D_N_=%d -D_SOURCE_TEMP_=%s -D_BOUNDARY_TEMP_=%s -D_MIN_DELTA_=%s -D_MAX_ITERATIONS_=%d -D_SEED_=%d"'%(TEST_PARAMETERS['N'], TEST_PARAMETERS['SOURCE_TEMP'], TEST_PARAMETERS['BOUNDARY_TEMP'], TEST_PARAMETERS['MIN_DELTA'], TEST_PARAMETERS['MAX_ITERATIONS'], TEST_PARAMETERS['D_SEED_'])
		try:
			print("\tCompiling...")
	 		print("\t" + test_case_compile_cmd[0:len(test_case_compile_cmd)/2] + "\n\t"  + test_case_compile_cmd[len(test_case_compile_cmd)/2:len(test_case_compile_cmd)])
	 		subprocess.check_output(shlex.split('make clean'))
	 		subprocess.check_output(shlex.split(test_case_compile_cmd))
		except:
			print('[Error in compilation] Running %s: %s' % (compile_cmd, sys.exc_info()))
			continue;

		#RUN
		try:
			print("\tRunning...")
			process = subprocess.Popen(shlex.split(EXECUTE_CMD), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
			stdout_output, stderr_output = process.communicate()
			print(stderr_output)
		except:
			print('[Error in execution] Running %s: %s' % (EXECUTE_CMD, sys.exc_info()))
			print(stderr_output)
			continue;

elapsed_time = time.time() - start_time
print('Ended at: %s. It takes %s'%(time.strftime("%c"),elapsed_time))
