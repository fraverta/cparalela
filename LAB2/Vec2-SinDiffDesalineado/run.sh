make clean; make CC=$1 CFLAGS="$2 -D_TEST_=result.txt"
./heat;
make clean; make CC=$1 CFLAGS="$2"
perf stat -e cpu-clock,cpu-cycles,page-faults,cache-references,cache-misses,instructions,stalled-cycles-frontend -r $3 ./heat 1> output.txt 2>&1
