#!/bin/bash

EXECUTE_CMD='perf stat -e cpu-clock,cpu-cycles,page-faults,cache-references,cache-misses,instructions,stalled-cycles-frontend -r 5 ./heat > output1.txt 2> output2.txt'
declare -a COMPILERS=('gcc-4.8' 'gcc-5' 'gcc-7' 'gcc-8' 'clang-3.7' 'clang-4.0'	'clang-5.0'	'clang-6.0')
declare -a SOURCES=('Lab1-ConDiff' 'Lab1-SinDiff' 'Vec1-ConDiff' 'Vec1-SinDiff' 'Vec1-SinDiffMod' 'Vec2-ConDiff' 'Vec2-SinDiff' 'Vec2-SinDiffDesalineado')
declare -a FLAGS=('-O3 -ffast-math -funroll-loops -ftree-vectorize' '-O3 -ffast-math -funroll-loops -fno-tree-vectorize')
declare -a SIZES=(500 1000 2000 3000 4000 5000)

for source in "${SOURCES[@]}"
do
	cd "$source";
	for compiler in "${COMPILERS[@]}"
	do
		i=0;
		for flag in "${FLAGS[@]}"
		do
			for N in "${SIZES[@]}"
			do
			 	echo "[$source $compiler $flag $N]"
				compile_cmd='make CC='"$compiler"' CFLAGS="-Wall -Wextra -std=c99 -g -march=native '"$flag"
				first_compilation="$compile_cmd -D_N_=$N\" OUTPUT=$compiler-$i-$N/"  
				echo $first_compilation;
				mkdir "$compiler-$i-$N";
				make clean; eval $first_compilation;
				cd "$compiler-$i-$N"; eval "$EXECUTE_CMD"; cd ..;
		i=$((i + 1))
		done
	done
	cd ..;
	done
done
