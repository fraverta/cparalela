'''
This program looks for the best optimization parameters for compiling a target program.
To achieve that it uses a genetical algorithm which was described in:

	Automatic Tuning of Compiler Optimizations and Analysis of their Impact
	By D. Plotnikov, D. Melnik, M. Vardanyan, R. Buchatskiy, R. Zhuykov, J. Lee
	Available at https://ac.els-cdn.com/S1877050913004419/1-s2.0-S1877050913004419-main.pdf?_tid=4b54bb46-5c3d-4170-a925-1c2736563e9c&acdnat=1523117133_1f25b727db5a2b0f291d9c430a6729a8

The authors also provides an implementation available here: https://github.com/ispras/tact/tree/f4a82ad6d4f1b905f28025b5d6ba14b50e82e79c

Author: Fernando Raverta
'''
# *********************************************************************************************
# PROGRAM PARAMETERS
# *********************************************************************************************
EXPERIMENT_TARGET_FOLDER_PATH = '/home/cp2018/cp201818'
PATH_TO_TARGET_SOURCE_CODE = '/home/cp2018/cp201818/cparalela/Original_heat/heat'
COMPILER = 'gcc-4.8'
BY_DEFAULT_ENABLED_FLAGS = '-Wall -Wextra -std=c99 -g -march=native -O2'
# ARGUMENTS FOR THE TARGET PROGRAM, LIKE TEST CASE PARAMETERS
RUNTIME_TARGET_PROGRAM_PARAMETERS = '-DRANDOM_SEED=10'
NUMBER_OF_REPETITION_FOR_AN_EXPERIMENT = 5
EXECUTABLE_PATH = '/home/cp2018/cp201818/cparalela/Original_heat/heat/heat'
######################################
### Genetical algorithm parameters ###
######################################

# NUMBER OF POPULATIONS IN THIS SEARCH
NUMBER_OF_POPULATION = 2

# NUMBER OF ENTITIES BELONGING TO A POPULATION
POPULATION_SIZE = 10

# NUMBER OF GENERATIONS THAT WILL BE EXPLORED
NUM_GENERATIONS = 8

# RATE OF BIRTHS THAT ARE MADE BY CROSSOVER TWO ENTITIES  IN A NEW GENERATION
CROSSOVER_RATE = 0.7

# PROBABILITY OF A SINGLE COMPILER OPTION TO CHANGE IN A MUTATION
SINGLE_OPTION_MUTATION_RATE = 0.1

# PROBABILITY OF A ENTITY MUTE AFTER CROSSOVER
AFTER_CROSSOVER_MUTATION_RATE = 0.03

# Probability of an element from different population be choosed by crossover outside its population.
MIGRATION_RATE = 0.5

# *********************************************************************************************
# *********************************************************************************************

import CompilerFlags
from Flags import *
from copy import copy, deepcopy
from Entity import Entity
import sys
import time

class Population:
    def __init__(self, number):
        # Population size
        self.size = POPULATION_SIZE
        # The number of entities that will be made by crossover from total new entities.
        self.cross_size = int(self.size * CROSSOVER_RATE)
        # The number of entities that will be made by mutation from total new entities.
        self.mutate_size = self.size - self.cross_size
        # Mutation probability of a single compiler option
        self.single_option_mutation_rate = SINGLE_OPTION_MUTATION_RATE
        # Mutation probability after crossover
        self.after_crossover_mutation_rate = AFTER_CROSSOVER_MUTATION_RATE
        # Probability of an element from different population be used by crossover outside its population.
        self.migration_rate = MIGRATION_RATE
        # The number of this population
        self.number = number
        # The entities which belong to this population
        self.entities = []
        # A register with best entities along all generations
        self.archive = []
        # Number of current generation
        self.generation = 0
        # Runtime target program paramters
        self.runtime_params = RUNTIME_TARGET_PROGRAM_PARAMETERS
        #
        self.average_performance = -1

    def init(self, compiler_options):
        self.entities = [Entity(compiler_options) for e in range(self.size)]
        for e in self.entities:
            e.init()
        self.update_archive()

    def estimate(self):
        for e in self.entities:
            e.estimate()

    def mutate(self):
        for i in range(self.mutate_size):
            entity = Entity(self.archive[randrange(len(self.archive))].compiler_options)
            entity.mutate(self.single_option_mutation_rate)
            self.entities.append(entity)

    def update_archive(self):
        new_archive = self.archive + self.entities
        new_archive.sort(key=lambda x: x.fitness)
        self.archive = new_archive[:self.size]
        self.entities = []
        self.average_performance = reduce(lambda x,y: x + y,map(lambda x: x.fitness, self.archive)) / self.size

    def cross(self, rest_of_archives):
        for i in range(self.cross_size):
            first_migrated = False
            # choosing of first entity for crossbreeding
            if random() < self.migration_rate and len(rest_of_archives) > 1:
                first = randrange(len(rest_of_archives))
                first_entity = rest_of_archives[first]
                first_migrated = True
            else:
                first = randrange(len(self.archive))
                first_entity = self.archive[first]

            # choosing of second entity for crossbreeding
            if random() < self.migration_rate and len(rest_of_archives) > 1:
                if first_migrated:
                    second = randrange(len(rest_of_archives))
                    while first == second:
                        second = randrange(len(rest_of_archives))
                else:
                    second = randrange(len(rest_of_archives))
                second_entity = rest_of_archives[second]
            else:
                if first_migrated:
                    second = randrange(len(self.archive))
                else:
                    second = randrange(len(self.archive))
                    while first == second:
                        second = randrange(len(self.archive))
                second_entity = self.archive[second]

            entity = Entity(first_entity.cross(second_entity.compiler_options))
            entity.mutate(self.after_crossover_mutation_rate)
            self.entities.append(entity)

    def breed(self, migration_file):
        self.mutate()
        self.cross(migration_file)
        self.generation += 1

    def run(self, migration_file):
        self.breed(migration_file)
        self.estimate()
        self.update_archive()

        # It could compute some statistics

#***********************************************************************************
#                          MAIN PROGRAM
#***********************************************************************************
now = time.strftime("%c")
sys.stdout = open(EXPERIMENT_TARGET_FOLDER_PATH+'/experiment-output-%s.txt'%''.join(now.split()), 'w+')
print('Started at: %s'%now)
#Set up Entity parameters
Entity.COMPILER = COMPILER
Entity.BY_DEFAULT_ENABLED_FLAGS =  BY_DEFAULT_ENABLED_FLAGS
Entity.PATH_TO_TARGET_SOURCE_CODE = PATH_TO_TARGET_SOURCE_CODE
Entity.NUMBER_OF_REPETITION_FOR_AN_EXPERIMENT = NUMBER_OF_REPETITION_FOR_AN_EXPERIMENT
Entity.RUNTIME_TARGET_PROGRAM_PARAMETERS = RUNTIME_TARGET_PROGRAM_PARAMETERS
Entity.EXECUTABLE_PATH = EXECUTABLE_PATH
compiler_options = []

for f in CompilerFlags.F_FLAGS:
    compiler_options.append(GCC_flag(f))

for m in CompilerFlags.M_FLAGS:
    compiler_options.append(GCC_mflag(m))

for enum in CompilerFlags.ENUM_FLAGS:
    compiler_options.append(GCC_enum(enum))

populations = [Population(i) for i in range(NUMBER_OF_POPULATION)]
for p in populations:
    p.init(compiler_options)

#Add knowed good entites
good_entity = Entity(compiler_options)
good_entity.compiler_options[0].value = False
good_entity.compiler_options[1].value = False
good_entity.compiler_options[2].value = False
good_entity.compiler_options[3].value = False

good_entity.compiler_options[4].value = True
good_entity.compiler_options[5].value = True
good_entity.compiler_options[6].value = True
good_entity.compiler_options[7].value = True
good_entity.compiler_options[8].value = True
good_entity.compiler_options[9].value = True
good_entity.compiler_options[10].value = True
good_entity.compiler_options[11].value = True
good_entity.compiler_options[12].value = True
good_entity.compiler_options[13].value = True
good_entity.compiler_options[14].value = True
good_entity.compiler_options[15].value = True
good_entity.compiler_options[16].value = True
good_entity.compiler_options[17].value = True

good_entity.compiler_options[18].value = False
good_entity.compiler_options[19].value = False

good_entity.compiler_options[20].value = 0
good_entity.compiler_options[21].value = 0

good_entity.estimate()
populations[0].archive.append(good_entity)\
populations[0].archive.sort(key=lambda x: x.fitness)
###


migration_archive = [copy(p.archive) for p in populations]
for generation in range(NUM_GENERATIONS):
    for p in populations:
        print '[Debug] Started population %d of %d, Generation %d of %d' % (p.number + 1, NUMBER_OF_POPULATION, p.generation + 1, NUM_GENERATIONS)
        migration_archive = reduce(lambda x,y:x+y,[copy(pp.archive) for pp in populations if pp != p])
        p.run(migration_archive)
        print '[Debug] Population %d Generation %d, AVERAGE PERFORMANCE: %f' %(p.number + 1, p.generation, p.average_performance)

result = []
for p in populations:
    result += p.archive

print("\n\n\n")
print("[RESULTS] For compiler %s we prove around of %d different compilation comands." %(COMPILER, len(Entity.LIST_OF_ENTITY_IDENTIFIER)))
print("We found the following best compiler parameters: ")
result.sort(key=lambda x: x.fitness)

i = 0
for e in result[:10]:
    print '\t%d'%i,'CPU-Clock (ms): %f' % e.fitness, 'Standard Desviation: %f'% e.perf_info['cpu-clock-sd'] + '%'
    print(str(e) + "\n")
    i += 1

print('Ended at: %s'%time.strftime("%c"))
