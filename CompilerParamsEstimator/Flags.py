from random import random, randrange

'''
GCC-flags are f- flags. They can be enabled or disabled.
'''
class GCC_flag:
    def __init__(self, name):
        self.name = name[2:len(name)]
        self.value = False

    def mutate(self):
        self.value = self.value != True

    def init(self):
        self.value = randrange(2) == 0

    def cross(self, second_option):
        if random() < 0.5:
            return self
        else:
            return second_option

    def __str__(self):
        return ('-f' if self.value else '-fno-') + self.name

'''
GCC-mflags are m- flags. They can be enabled or disabled.
'''
class GCC_mflag(GCC_flag):
    def __str__(self):
        return ('-m' if self.value else '-mno-') + self.name

'''
GCC-enum are GCC flags that will has exactly one value from all possibilities
'''
class GCC_enum(GCC_flag):
    def __init__(self, enum_values):
        self.name = "GCC_enum"
        self.enum_values = enum_values
        self.value = 0

    def mutate(self):
        self.value = randrange(len(self.enum_values))

    def init(self):
        self.value = randrange(len(self.enum_values))

    def __str__(self):
        return ('-' + self.enum_values[self.value]) if self.enum_values[self.value] != '' else ''


class GCC_param(GCC_flag):
    def __init__(self):
        print "GCC_param is not supported yet"
        exit(1)
