#SIMPLE TYPE -> NAME WHEN ENABLED, EMPTY STRING WHEN NOT
#BINARY FLAGS: ENABLED,NOT ENABLED  -m flags

F_FLAGS =[
'f-defer-pop',
'f-forward-propagate',
'f-optimize-sibling-calls',
'f-optimize-strlen',

#-03 FLAGS
'f-inline-functions',
'f-unswitch-loops',
'f-predictive-commoning',
'f-gcse-after-reload',
#f-tree-loop-vectorize
'f-tree-loop-distribution',
'f-tree-loop-distribute-patterns',
'f-loop-interchange',
#f-split-paths
'f-tree-slp-vectorize',
'f-vect-cost-model',
'f-tree-partial-pre',
'f-peel-loops',
'f-ipa-cp-clone',

#Flag reported by paper
'f-prefetch-loop-arrays',
'f-tree-vectorize',
]

M_FLAGS = []
ENUM_FLAGS = [
['ffast-math',''],
['funroll-loops', ''],
]
