from copy import copy
import shlex, subprocess
from random import random
import sys
class Entity:
    PATH_TO_TARGET_SOURCE_CODE = ''
    COMPILER = ''
    BY_DEFAULT_ENABLED_FLAGS = ''
    NUMBER_OF_REPETITION_FOR_AN_EXPERIMENT = 0
    RUNTIME_TARGET_PROGRAM_PARAMETERS = 0
    EXECUTABLE_PATH = ''

    # List to record different commands we've proved. See get_int_identifier.
    LIST_OF_ENTITY_IDENTIFIER = []

    def __init__(self, compiler_options):
        # The value os function fitness measure in this entity (tipically execution time)
        self.fitness = float('inf')
        # Number of generation?
        self.gen_number = 0
        # Number of runs
        self.run_number = 0
        # Name of executable file
        self.file_name = ''
        # Compiler options
        self.compiler_options = [copy(p) for p in compiler_options]
        # Perf information after run
        self.perf_info = {}

    def init(self):
        for c in self.compiler_options:
            c.init()

    def estimate(self):
        # Compile
        compile_cmd = str(self)
        print('[Compiling] %s'%(compile_cmd))
        try:
            subprocess.check_output(shlex.split(compile_cmd))
        except:
            print('[Error] Running %s: %s' % (compile_cmd, sys.exc_info()))
            self.fitness = float('inf')
            return

        run_cmd = "perf stat -e cpu-clock -r %d %s" % (Entity.NUMBER_OF_REPETITION_FOR_AN_EXPERIMENT,Entity.EXECUTABLE_PATH)
        print('[Running] %s' % run_cmd)
        try:
            process = subprocess.Popen(shlex.split(run_cmd), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            stdout_output, stderr_output = process.communicate()
            #print(stdout_output)
            print(stderr_output)
            result = stderr_output.split()
            while ('run' not in result[0]):
                del result[0]
            del result[0]
            cpu_clock_ms=float(result[0].replace(',', '.'))
            print cpu_clock_ms
            del result[0]
            while ('+-' not in result[0]):
                del result[0]
            del result[0]
            cpu_clock_sd = float(result[0].replace('%', '').replace(',', '.'))
            print cpu_clock_sd
            self.perf_info = {
                'cpu-clock-ms': cpu_clock_ms,
                'cpu-clock-sd': cpu_clock_sd,
                'raw_string': result
            }
            self.fitness = self.perf_info['cpu-clock-ms']
        except:
            print('[Error] Running %s: %s' % (run_cmd, sys.exc_info()))
            self.fitness = float('inf')

    def mutate(self, mutation_rate):
        for o in self.compiler_options:
            if random() < mutation_rate:
                o.mutate()

        id = self.get_int_identifier()
        if id not in Entity.LIST_OF_ENTITY_IDENTIFIER:
            Entity.LIST_OF_ENTITY_IDENTIFIER.append(id)

    def cross(self, second_entity):
        new_options = []
        for i in range(len(self.compiler_options)):
            new_options.append(self.compiler_options[i].cross(second_entity[i]))

        return new_options

    def __str__(self):
        compile_cmd = 'make -C %s CC=%s CFLAGS="%s %s ' % (Entity.PATH_TO_TARGET_SOURCE_CODE, Entity.COMPILER, Entity.RUNTIME_TARGET_PROGRAM_PARAMETERS,Entity.BY_DEFAULT_ENABLED_FLAGS)
        compile_cmd += ' '.join(map(str, self.compiler_options)) + '"'
        return compile_cmd

    '''
    The intention of this method is to return a unique integer representation of this entity based on enable/disable flag as binary number.
    It has some problems, because flag values may not be binary. Also, if there had more than 63 flags it would produce very long numbers.
    (Though it is not a really problem in Python, because it allows integer of arbirary magnitiude).
    But, it isn't crucial for us because We want to have a vague idea about how many differents commands we tried.
    By the way, the world is not perfect but We have to live here.
    '''
    def get_int_identifier(self):
        i=0
        result=0
        for c in self.compiler_options:
            if c.value:
                result += 2**i
            i+=1
        return result