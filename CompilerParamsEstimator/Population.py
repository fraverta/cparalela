from Entity import Entity
from random import random, randrange

class Population:
    def __init__(self, number):
        # Population size
        self.size = POPULATION_SIZE
        # The number of entities that will be made by crossover from total new entities.
        self.cross_size = self.size * CROSSOVER_RATE
        # The number of entities that will be made by mutation from total new entities.
        self.mutate_size = self.size - self.cross_size
        # Mutation probability of a single compiler option
        self.single_option_mutation_rate = SINGLE_OPTION_MUTATION_RATE
        # Mutation probability after crossover
        self.after_crossover_mutation_rate = AFTER_CROSSOVER_MUTATION_RATE
        # Probability of an element from different population be used by crossover outside its population.
        self.migration_rate = MIGRATION_RATE
        # The number of this population
        self.number = number
        # The entities which belong to this population
        self.entities = []
        # A register with best entities along all generations
        self.archive = []
        # Number of current generation
        self.generation = 1
        # Runtime target program paramters
        self.runtime_params = RUNTIME_TARGET_PROGRAM_PARAMETERS

    def init(self, compiler_options):
        self.entities = [Entity(compiler_options) for e in range(self.size)]
        for e in self.entities:
            e.init()
        self.update_archive()

    def estimate(self):
        for e in self.entities:
            e.estimate()

    def mutate(self):
        entity = Entity(self.archive[randrange(len(self.archive))].compiler_options)
        entity.mutate(self.single_option_mutation_rate)
        self.entities.append(entity)

    def update_archive(self):
        new_archive = self.archive + self.entities
        new_archive.sort(key=lambda x: x.fitness, reverse=True)
        self.archive = new_archive[:self.size]
        self.entities = []

    def cross(self, rest_of_archives):
        first_migrated = False
        # choosing of first entity for crossbreeding
        if random() < self.migration_rate and len(rest_of_archives) > 1:
            first = randrange(len(rest_of_archives))
            first_entity = rest_of_archives[first]
            first_migrated = True
        else:
            first = randrange(len(self.archive))
            first_entity = self.archive[first]

        # choosing of second entity for crossbreeding
        if random() < self.migration_rate and len(rest_of_archives) > 1:
            if first_migrated:
                second = randrange(len(rest_of_archives))
                while first == second:
                    second = randrange(len(rest_of_archives))
            else:
                second = randrange(len(rest_of_archives))
            second_entity = rest_of_archives[second]
        else:
            if first_migrated:
                second = randrange(len(self.archive))
            else:
                second = randrange(len(self.archive))
                while first == second:
                    second = randrange(len(self.archive))
            second_entity = self.archive[second]

        entity = Entity(first_entity.cross(second_entity.compiler_options))
        entity.mutate(self.after_crossover_mutation_rate)
        self.entities.append(entity)

    def breed(self, migration_file):
        self.mutate()
        self.cross(migration_file)
        self.generation += 1

    def run(self, migration_file):
        self.breed(migration_file)
        self.estimate()
        self.update_archive()

        # It could compute some statistics
